use termion::event::Key;

use tui::backend::Backend;
use tui::terminal::Frame;
use tui::widgets::{Widget, Block, Borders, Paragraph, Text};
use tui::layout::{Layout, Constraint, Direction, Rect};
use tui::style::{Style, Color};


#[derive(Clone)]
pub struct CommandLineState<T, E> {
    pub current_input: String,
    pub fuzzy_expansion: String,
    pub fuzz_suggestions: Vec<(String, Vec<bool>)>,
    pub last_error: Option<E>,
    pub highlighted: Vec<T>,
}
impl<T, E> CommandLineState<T, E> {
    pub fn new() -> Self {
        Self {
            current_input: String::new(),
            fuzzy_expansion: String::new(),
            fuzz_suggestions: vec![],
            last_error: None,
            highlighted: vec![]
        }
    }
}
impl<T, E> Default for CommandLineState<T, E> {
    fn default() -> Self {
        Self {
            current_input: String::default(),
            fuzzy_expansion: String::default(),
            fuzz_suggestions: Vec::default(),
            last_error: None,
            highlighted: Vec::default(),
        }
    }
}

pub fn handle_key_input<M>(
    mut current_text: String,
    key: Key,
    on_close: M,
    on_accept: M,
    on_text_change: impl Fn(&str) -> M
) -> Option<M> {
    match key {
        Key::Backspace => {
            // Remove one character. We don't care about the result
            // since non-empty string can't be erased
            let _ = current_text.pop();
            Some(on_text_change(&current_text))
        },
        Key::Char('\n') => {
            Some(on_accept)
        }
        Key::Char(c) => {
            current_text.push(c);
            Some(on_text_change(&current_text))
        }
        Key::Esc => {
            Some(on_close)
        }
        _ => None
    }
}

// Returns the desired location of the cursor in the specified area
pub fn draw_command_line<T, E: std::fmt::Debug>(
    f: &mut Frame<impl Backend>,
    area: Rect,
    cli: &CommandLineState<T, E>
) -> (u16, u16) {
    let mut block = Block::default()
        .border_style(Style::default().fg(Color::Gray))
        .borders(Borders::TOP);
    block.render(f, area);

    let chunks = Layout::default()
        .direction(Direction::Vertical)
        .constraints([
            Constraint::Length(1),
            Constraint::Length(1),
            Constraint::Min(1)
        ].as_ref())
        .split(block.inner(area));

    // Draw the current input
    Paragraph::new([Text::raw(cli.current_input.clone())].iter())
        .wrap(true)
        .render(f, chunks[0]);

    // Draw the fuzzy expanded input
    Paragraph::new([
            Text::Styled(
                cli.fuzzy_expansion.clone().into(),
                Style::default().fg(Color::Yellow)
            ),
            cli.last_error.as_ref().map(|e| {
                Text::Styled(
                    format!(" >> {:?}", e).into(),
                    Style::default().fg(Color::Red)
                )
            }).unwrap_or_else(||Text::raw(""))
        ].iter())
        .wrap(false)
        .render(f, chunks[1]);

    // Draw the result of fuzzy expansion
    let suggestion_text = cli.fuzz_suggestions.iter()
        .map(|(suggestion, matching)| {
            suggestion.chars().zip(matching)
                .map(|(c, m)| {
                    let style = if *m {
                        Style::default().fg(Color::Green)
                    }
                    else {
                        Style::default().fg(Color::Gray)
                    };
                    Text::styled(c.to_string(), style)
                })
                .chain(vec![Text::raw("\n".to_string())].into_iter())
        })
        .flatten()
        .collect::<Vec<_>>();
    Paragraph::new(suggestion_text.iter())
        .render(f, chunks[2]);

    (chunks[0].left() + cli.current_input.chars().count() as u16, chunks[0].top())
}
